package com.allstate.services;

import com.allstate.dao.PaymentDao;
import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentDao dao;


    @Override
    public int rowcount() {
        return dao.rowcount();
    }

    @Override
    public Payment findById(int id) {
        return dao.findByID(id);
    }

    @Override
    public List<Payment> findByType(String type) {
        return dao.findByType(type);
    }

    @Override
    public int save(Payment payment) {
        return dao.save(payment);
    }

    @Override
    public List<Payment> getAll() {
        return dao.getAllPayments();
    }


    @Override
    public int update(Payment payment) {
        return dao.update(payment);
    }

}
