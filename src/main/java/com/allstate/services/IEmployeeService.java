package com.allstate.services;

import com.allstate.entities.Employee;

import java.util.List;

public interface IEmployeeService {
    Long getTotal();

    List<Employee> all();

    void save(Employee employee);

    Employee find(int id);
}
