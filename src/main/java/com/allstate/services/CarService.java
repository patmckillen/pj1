package com.allstate.services;

import com.allstate.dao.CarRepo;
import com.allstate.entities.Car;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarService implements ICarService {

    @Autowired
    private CarRepo carRepo;


    @Override
    public List<Car> all() {
        return carRepo.findAll();
    }

    @Override
    public Optional<Car> find(ObjectId id) {
        return  carRepo.findById(id);
    }
}
