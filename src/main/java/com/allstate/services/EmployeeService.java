package com.allstate.services;

import java.util.List;

import com.allstate.dao.IEmployeeData;
import com.allstate.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService implements IEmployeeService {
    @Autowired
    private IEmployeeData dao;

    @Override
    public Long getTotal(){
       return dao.getTotal();
    }
    @Override
    public List<Employee> all()
    {
        return dao.findAll();
    }
    @Override
    public void save(Employee employee)
    {
        dao.save(employee);
    }
    @Override
    public Employee find(int id)
    {
        if (id<=0)
        {
            return null;
        }
        else
        {
            return dao.findById(id);
        }

    }

}
