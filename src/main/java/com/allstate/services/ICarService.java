package com.allstate.services;

import com.allstate.entities.Car;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface ICarService {

    List<Car> all();
    Optional<Car> find(ObjectId id);
}
