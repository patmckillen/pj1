package com.allstate.utils;

public  class General {
    public static double convertEurToSterling(double amt)
    {
        return amt*1.02;
    }

    public static double convertEurToDollar(double amt)
    {
        return amt*1.05;   
    }
}
