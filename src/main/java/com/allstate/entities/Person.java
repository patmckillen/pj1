package com.allstate.entities;

//import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public abstract class Person {
    @Id
    private int id;
    private String email;
     
    public Person()
    {
        
    }
    public Person(int id, String email){
        this.id =id;
        this.email=email;       
    }

     public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    protected void print()
    {
        System.out.printf("Values are %s,  %d" , this.email, this.id);
    }


    public abstract void display();

   // {

   //     System.out.printf("Display Values are %s,  %d" , this.email, this.id);
   // }
   

}
