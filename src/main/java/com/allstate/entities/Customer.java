package com.allstate.entities;

public class Customer extends Person {
   private  String name;
   private  String address;

   public String getName() {
       return name;
   }

   public void setName(String name) {
       this.name = name;
   }

   public String getAddress() {
       return address;
   }

   public void setAddress(String address) {
       this.address = address;
   }

   public Customer(String name, String address) {
       this.name = name;
       this.address = address;
   }

   public Customer(int id, String email, String name, String address) {
    super(id, email);
    this.name = name;
    this.address = address;
   }

 

   @Override
   public void display() {
       // TODO Auto-generated method stub
       System.err.printf("Customer values %s, %d, %s", super.getEmail(), super.getId(), this.name);
   }

  

}
