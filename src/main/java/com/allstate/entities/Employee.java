package com.allstate.entities;

import com.allstate.exceptions.EmployeeException;

//person - super, Employee - Subclass 
//public, protected members
//do not inherit constructors
//Quiz - static, constructors, abstract, modifiers
public final class Employee extends Person {

    private String fname;
    private String lname;
    private double salary;
    //private static final double TAX_RATE =.47;
    private static  double taxrate;

  
    public Employee(int id, String email, String fname, String lname, double salary)
    {
    
       
        this(id, email, fname, lname);
        this.salary=salary;

    }

    public Employee(int id, String email, String fname, String lname)
    {
        super(id,email);
        this.fname=fname;
        this.lname=lname;

    }

    public Employee()
    {
        super();
    }
    




    public void setFname(String fname) {
        this.fname = fname;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        if(salary<0 )
        {
            throw new EmployeeException("Salary > 0");   
        }
        else
        {
            this.salary = salary;
        }
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname()
    {
        return this.fname;
    }

   
    public void AddBonus()
    {
        this.salary +=100;
    }

   
    public void AddBonus(double amt)
    {
        this.salary +=amt;
    }

    public static double getTaxrate() {
        return taxrate;
    }


 
    public static void setTaxrate(double taxrate) {
        Employee.taxrate = taxrate;
    }


  
    @Override
    public void display() {
        
      //  super.display();
      System.err.printf("Employee values %s, %d, %s", super.getEmail(), super.getId(), this.fname);

    }


}
