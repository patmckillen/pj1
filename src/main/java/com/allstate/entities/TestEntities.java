package com.allstate.entities;

import java.util.Set;
import java.util.TreeSet;



public class TestEntities {
    public static void main1(String[] args) {
    //Person person = new Employee(1, "d@i","dee", "geart");
      //  person.display();
    

       // Employee employee = new Employee(1,"d@i.ie","deirdre","geary");
      //  employee.display();

      //  Customer customer = new Customer(1,"d@i.ie","dee", "clare");

        //employee.display();

       // Person[] people = new Person[2];

       // people[0]=employee;
       // people[1]=customer;

       // for (int i=0; i<people.length;i++)
       // {
        //    people[i].display();
       // }
//quiz

      //  List<Employee> lst = new ArrayList<Employee>();
       // lst.add( new Employee(1,"d@i.ie","aoife","geary"));
      //  lst.add( new Employee(2,"dee@i.ie","dee","geary"));
      //  lst.add( new Employee(3,"deirdre@i.ie","deirdre","coleman"));

        // lst.forEach( (emp) -> {
        //       System.out.println(emp.getFname());
        // }    
        // );

        // lst.removeIf((emp)->emp.getId()==2);

        // lst.forEach( (emp) -> {
        //   System.out.println(emp.getFname());
        // });





        //treeset
       // Set<Employee> tlst = new TreeSet<Employee>(new EmployeeComparator());
       Set<Employee> tlst = new TreeSet<Employee>((e1,e2)-> {
         return (e1.getId() - e2.getId());
       });

        tlst.add( new Employee(100,"d@i.ie","Jim","geary"));
        tlst.add( new Employee(20,"dee@i.ie","Joan","geary"));
        tlst.add( new Employee(11,"deirdre@i.ie","Bill","coleman"));

        tlst.forEach( (emp) -> {
          System.out.println(emp.getFname() + "," + emp.getId());
        });  
        
        

}//main
}//testentities

 class EmployeeComparator implements java.util.Comparator <Employee> {
  public int compare(Employee p1, Employee p2) {
    //return (p1.getId) - p2.getId());
    return (p1.getFname().compareTo(p2.getFname()));
  }
}

