package com.allstate.demo;


import java.util.Arrays;
import java.util.Iterator;
import java.util.TreeSet;

public class TestAddress {

    public static void main(String[] args)
    {

    TreeSet<Address> treeSet = new TreeSet<Address>();
        treeSet.add(   new Address(4, "Baker bb Avenue"));
        treeSet.add(    new Address(2, "Hollywood Street"));
        treeSet.add(    new Address(2, "Baker Avenue"));
        treeSet.add(   new Address(2, "Baker Avenue"));
        treeSet.add(     new Address(3, "Paradise St"));

        Iterator iterator = treeSet.iterator();
        while (iterator.hasNext())
            System.out.print(iterator.next());
}

    static void log(String msg)
    {
        System.out.println(msg);
    }
}




