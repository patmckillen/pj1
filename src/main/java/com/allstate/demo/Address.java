package com.allstate.demo;

public class Address  implements Comparable<Address>
{
    int aptNo;
    String streetName;

    @Override
    public int hashCode() { return aptNo; }

    @Override
    public boolean equals(Object obj)
    {
        Address other = (Address) obj;
        if (aptNo == other.aptNo)
            return true;
        return false;
    }

    @Override
    public int compareTo(Address other)
    {
        return (aptNo - other.aptNo);
    }

    @Override
    public String toString() { return "[" + aptNo + ", " + streetName + "]"; }

    public Address(int aptNo, String streetName)
    {
        this.aptNo = aptNo;
        this.streetName = streetName;
    }
}