package com.allstate.demo;
import java.io.*;


public class FileCopier {

  //Write a method, infile, outfile - read infile and write to another file
  
  public void Copy(String inFile, String outFile) 
    {
        try
        {
        FileReader reader = new FileReader(inFile);
        BufferedReader bReader = new BufferedReader(reader);

        FileWriter writer  = new FileWriter(outFile);
        BufferedWriter bWriter = new BufferedWriter(writer);

        while (true)
        {
            String line = bReader.readLine();
            if (line == null) break;
            bWriter.write(line);
            bWriter.newLine();

        }
        bWriter.flush();
        bWriter.close();
        bReader.close();


        }
        catch (Exception e) {
            
        }

    }

}
