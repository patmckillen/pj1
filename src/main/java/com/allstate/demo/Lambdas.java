package com.allstate.demo;

public class Lambdas {
    public static void main1(String[] args) {


        GreetingService greetingService = (s)->{
                System.out.println("Hello " + s);
        };

        greetingService.sayMessage("deirdre");

        GreetingServiceExtra greetingServiceExtra = (m,d)->{

                return "Hello " + m + d;
        };
        System.out.println(greetingServiceExtra.sayMessage("deirdre", " again"));


        
    }

    interface GreetingService {
        void sayMessage(String message);
     }

     interface GreetingServiceExtra {
        String sayMessage(String message, String detail);
     }


}
