package com.allstate.dao;

import java.util.List;

import com.allstate.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeRepoMongoImpl implements IEmployeeData {

    @Autowired
    private MongoTemplate tpl;

    @Override
    public Long getTotal() {
        Query query = new Query();
        Long result= tpl.count(query, Employee.class);
        return result;


    }

    @Override
    public Employee findById(int id) {
        // TODO Auto-generated method stub

        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Employee employee = tpl.findOne(query, Employee.class);

        return employee;
    }
    @Override
    public List<Employee> findAll() {
        // TODO Auto-generated method stub
        return tpl.findAll(Employee.class);
    }
    @Override
    public void save(Employee employee) {
        // TODO Auto-generated method stub
        tpl.insert(employee);
    }

    @Override
    public void update(Employee employee) {
        // TODO Auto-generated method stub

    }
    
}
