package com.allstate.dao;

import java.util.List;

import com.allstate.entities.Employee;

public interface IEmployeeData {
    Long getTotal();
    Employee findById(int id);
    List<Employee> findAll();
    void save(Employee employee);
    void update(Employee employee);
    
}
