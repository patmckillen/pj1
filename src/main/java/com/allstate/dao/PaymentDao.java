package com.allstate.dao;

import com.allstate.entities.Payment;

import java.util.Collection;
import java.util.List;

public interface PaymentDao {

        int rowcount();
        Payment findByID(int id);
        List<Payment> findByType(String type);
        int save(Payment payment);
        List<Payment> getAllPayments();
        int update(Payment payment);
}
