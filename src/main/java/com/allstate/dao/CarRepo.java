package com.allstate.dao;

import com.allstate.entities.Car;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CarRepo extends MongoRepository<Car, ObjectId> {


}
