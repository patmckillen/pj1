package com.allstate.rest;

import com.allstate.entities.Payment;
import com.allstate.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/payments")
public class PaymentController {
    @Autowired
    private PaymentService paymentService;
    Logger logger = Logger.getLogger(PaymentController.class.getName());

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus()
    {
        logger.info("Payment status method");
        return "Payment Rest Api is running";
    }

    //not required all below but wanted to check what was in the db over the API
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Payment> getAllPayments() {

        return paymentService.getAll();
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public int rowcount() {

        return paymentService.rowcount();
    }

    @RequestMapping(value = "/findbyid/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> findbyid(@PathVariable("id") int id) {
        Payment payment = paymentService.findById(id);

        if (payment == null){
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/findbytype/{type}", method = RequestMethod.GET)
    public ResponseEntity<List> findbyid(@PathVariable("type") String type) {
        List<Payment> paymentList= paymentService.findByType(type);

        if (paymentList.size() == 0){
             return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List>(paymentList,HttpStatus.OK);
        }
    }
    @RequestMapping(value = "save" ,method = RequestMethod.POST)
    public int addPayment(@RequestBody Payment payment) {
        return paymentService.save(payment);
    }


    @RequestMapping(value = "update" ,method = RequestMethod.POST)
    public int update(@RequestBody Payment payment) {
        return paymentService.update(payment);
    }

}
