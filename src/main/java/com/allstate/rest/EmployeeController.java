package com.allstate.rest;

import com.allstate.entities.Employee;
import com.allstate.services.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    IEmployeeService service;

    @RequestMapping(value = "/total", method = RequestMethod.GET)
    public Long getTotal()
    {
        return  service.getTotal();
    }
    @RequestMapping(value = "/save", method = RequestMethod.POST )
    public void save(@RequestBody Employee employee)
    {
        service.save(employee);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Employee> getAll()
    {
        return  service.all();
    }

    @RequestMapping(value="/find/{id}", method=RequestMethod.GET)
    public ResponseEntity<Employee> getPersonByIdHandling404(@PathVariable("id") int id) {

        Employee employee = service.find(id);

        if (employee ==null) {
            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<Employee>(employee, HttpStatus.OK);
        }
    }




}
