package com.allstate.exceptions;

public class EmployeeException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 3538181174213580960L;

    public EmployeeException(String message)
   {
       super(message);
   }
    

}
