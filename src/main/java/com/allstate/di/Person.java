package com.allstate.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class Person {
    private int id;
    private int age;

    @Autowired
    private Pet pet;

    public Person(Pet pet) {
        this.pet = pet;
    }

    public Person(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Pet getPet() {

        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }
}
