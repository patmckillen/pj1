package com.allstate.dao;

import com.allstate.entities.Employee;
//import org.junit.Test;


import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

@SpringBootTest
public class RepoMongoTest {
    @Autowired
    IEmployeeData dao;

    @Autowired
    MongoTemplate mongoTemplate;

    @Test
    public void testSaveEmployeeDocument() {
        Employee employee = new Employee(44, "d@i.ie", "deirdre", "geary");
        dao.save(employee);

        employee = dao.findById(44);

        Assert.assertNotNull("Check if employee is found", employee);


    }


    @AfterEach
    public  void cleanUp() {
        for (String collectionName : mongoTemplate.getCollectionNames()) {
            if (!collectionName.startsWith("system.")) {
                mongoTemplate.dropCollection(collectionName);
            }
        }

    }
}

